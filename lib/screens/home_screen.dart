import 'package:carousel/screens/page_views/product_screen.dart';
import 'package:flutter/material.dart';

import 'favorite_screen.dart';

class HomeScreen extends StatefulWidget {
  final List<Map<String, dynamic>> products;
  final Function(String id) onClickLikeButton;

  const HomeScreen({Key? key, required this.products, required this.onClickLikeButton}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final PageController _pageController = PageController(
    initialPage: 0,
  );
  
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: PageView.builder(
        scrollDirection: Axis.vertical,
        controller: _pageController,
        onPageChanged: ((value) => setState(() => _currentIndex = value)),
        itemCount: widget.products.length,
        itemBuilder: (context, index) {
          return ProductScreen(
            product: widget.products[index],
            onClickLikeButton: widget.onClickLikeButton,
          );
        },
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      floatingActionButton: SizedBox(
        height: 50,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
              style: TextButton.styleFrom(
                textStyle: const TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                primary: Colors.black,
              ),
              child: const Text('Favoris'),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FavoriteScreen(products: widget.products),
                  ),
                );
              },
            ),
          ]
        ),
      ),
    );
  }
}
