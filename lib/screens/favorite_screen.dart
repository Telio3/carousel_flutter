import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavoriteScreen extends StatefulWidget {
  final List<Map<String, dynamic>> products;

  const FavoriteScreen({Key? key, required this.products}) : super(key: key);

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  final List<Map<String, dynamic>> likedProducts = [];

  @override
  void initState() {
    _getlikedProductsId();
    super.initState();
  }

  void _getlikedProductsId() async {
    likedProducts.clear();
    
    final prefs = await SharedPreferences.getInstance();
    List<String> likedProductsId = prefs.getStringList('likedProductsId') ?? [];

    for (final product in widget.products) {
      if (likedProductsId.contains(product['id'])) {
        likedProducts.add(product);
      }
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: const IconThemeData(
          color: Colors.black
        ),
        title: const Text('Favoris', style: TextStyle(
          color: Colors.black
        )),
      ),

      body: GridView.builder(
        padding: const EdgeInsets.all(10),
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 40,
          crossAxisSpacing: 24,
          childAspectRatio: 0.75,
        ),
        itemCount: likedProducts.length,
        itemBuilder: (context, index) {
          var product = likedProducts[index];

          return Column(
            children: [
              SizedBox(
                height: 200,
                width: 200,
                child: Stack(
                  children: [
                    Positioned.fill(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: SizedBox.fromSize(
                          size: const Size.fromRadius(48),
                          child: Image.asset(product['images'][0], fit: BoxFit.cover),
                        ),
                      )
                    ),
                    Positioned(
                      bottom: 10,
                      right: 10,
                      child: CircleAvatar(
                        radius: 20,
                        backgroundColor: Colors.white,
                        child: IconButton(
                          icon: const Icon(Icons.favorite),
                          color:Colors.red,
                          onPressed: () {},
                        ),
                      ),
                    ),
                  ]
                )
              ),
              
              Padding(
                padding: const EdgeInsets.only(left: 10.0, top: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(product['title'], style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                    )),
                  ],
                )
              ),
            ],
          );
        },
      ),
    );
  }
}