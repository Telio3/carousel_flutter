import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductScreen extends StatefulWidget {
  final Map<String, dynamic> product;
  final Function(String id) onClickLikeButton;

  const ProductScreen({Key? key, required this.product, required this.onClickLikeButton}) : super(key: key);

  @override
  State<ProductScreen> createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  bool _isLiked = false;

  final PageController _pageController = PageController(
    initialPage: 0
  );

  int _currentIndex = 0;

  @override
  void initState() {
    _knowIfProductLiked();
    super.initState();
  }

  void _knowIfProductLiked() async {
    final prefs = await SharedPreferences.getInstance();
    List<String> likedProductsId = prefs.getStringList('likedProductsId') ?? [];

    if (likedProductsId.contains(widget.product['id'].toString())) {
      setState(() {
        _isLiked = true;
      });
    }
  }

  List<Widget> _indicators(imagesLength, currentIndex) {
    return List<Widget>.generate(imagesLength, (index) {
      return Container(
        margin: const EdgeInsets.all(3),
        width: 10,
        height: 10,
        decoration: BoxDecoration(
            color: currentIndex == index ? Colors.black : Colors.black26,
            shape: BoxShape.circle),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [

        PageView.builder(
          itemCount: widget.product['images'].length,
          pageSnapping: true,
          controller: _pageController,
          onPageChanged: (page) {
            setState(() {
              _currentIndex = page;
            });
          },
          itemBuilder: (context, index) {
            return Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(widget.product['images'][index]),
                  fit: BoxFit.cover
                ),
              ),
            );
          }
        ),

        Positioned(
          child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: Container(
              color: Colors.white38,
              height: MediaQuery.of(context).size.height * 0.15,
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: _indicators(widget.product['images'].length , _currentIndex)
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.6,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.product['title'],
                                style: const TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black
                                ),
                              ),
                              Text(
                                widget.product['description'],
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: const TextStyle(
                                  fontSize: 20,
                                  color: Colors.black
                                ),
                              ),
                            ],
                          ),
                        ),

                        CircleAvatar(
                          radius: 30,
                          backgroundColor: Colors.white,
                          child: IconButton(
                            icon: const Icon(Icons.favorite),
                            color: _isLiked ? Colors.red : Colors.black,
                            onPressed: () {
                              setState(() => _isLiked = !_isLiked);
                              widget.onClickLikeButton(widget.product['id']);

                              var state = _isLiked ? 'added' : 'removed';
                              
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(widget.product['title'] + ' is ' + state + ' to favorites'),
                                  duration: const Duration(milliseconds: 500),
                                )
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ),
          ),
        ),
      ],
    );
  }
}
