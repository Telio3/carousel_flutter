import 'package:carousel/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

const List<Map<String, dynamic>> products = [
  {
    'id': '1',
    'title': 'Zelda',
    'description': 'The Legend of Zelda: Breath of the Wild',
    'images': [
      'assets/images/1.jpg',
      'assets/images/2.jpg',
      'assets/images/3.jpg'
    ]
  },
  {
    'id': '2',
    'title': 'Horizon',
    'description': 'Horizon Forbidden West',
    'images': [
      'assets/images/4.jpg',
      'assets/images/5.jpg',
      'assets/images/6.jpg'
    ]
  },
  {
    'id': '3',
    'title': 'Rocket League',
    'description': 'Voiture Ballon wéééééé',
    'images': [
      'assets/images/7.jpg',
      'assets/images/8.jpg',
      'assets/images/9.jpg'
    ]
  }
];

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  void _onClickLikeButton(String id) async {
    final prefs = await SharedPreferences.getInstance();

    final List<String> likedProductsId = prefs.getStringList('likedProductsId') ?? [];

    if (likedProductsId.contains(id.toString())) {
      likedProductsId.remove(id.toString());
    } else {
      likedProductsId.add(id.toString());
    }

    await prefs.setStringList('likedProductsId', likedProductsId);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Carousel',
      home: HomeScreen(products: products, onClickLikeButton: (id) { _onClickLikeButton(id); }),
      debugShowCheckedModeBanner: false,
    );
  }
}
